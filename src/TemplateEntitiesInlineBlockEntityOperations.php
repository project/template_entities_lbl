<?php

namespace Drupal\template_entities_lbl;

use Drupal\Core\Entity\EntityInterface;
use Drupal\layout_builder\InlineBlockEntityOperations;
use Drupal\template_entities\Entity\Template;
use Drupal\template_entities\Entity\TemplateType;

/**
 * Subclass of InlineBlockEntityOperations to duplicate inline blocks
 * when saving duplicate based on shared library layout. The parent method
 * has logic in originalEntityUsesDefaultStorage($entity) which will only
 * duplicate if on an entity update and when the default layout is being
 * overridden.
 *
 * @internal
 *   This is an internal utility class wrapping hook implementations.
 */
class TemplateEntitiesInlineBlockEntityOperations extends InlineBlockEntityOperations {

  /**
   * Handles saving a parent entity after created from template.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The parent entity.
   */
  public function handlePreSave(EntityInterface $entity) {
    if (!isset($entity->template) || !$entity->isNew()) {
      return;
    }

    if (!$this->isLayoutCompatibleEntity($entity)) {
      return;
    }

    if (isset($entity->template) && $entity->template instanceof Template) {
      $template_type = TemplateType::load($entity->template->bundle());
      if ($template_type->getThirdPartySetting('template_entities_lbl', 'override_library_layout', false)) {
        if ($sections = $this->getEntitySections($entity)) {
          foreach ($this->getInlineBlockComponents($sections) as $component) {
            $this->saveInlineBlockComponent($entity, $component, FALSE, TRUE);
          }
        }
      }
    }
  }

}
