CONTENTS OF THIS FILE
---------------------

 * Overview
 * Requirements
 * Installation
 * Usage
 * Maintainers


OVERVIEW
--------

This is an add-on module to [Template Entities](https://www.drupal.org/project/template_entities) to enhance use of Layout Builder Library layouts in templates. It adds an option to template type configuration that, when checked, will override Layout Builder Library layouts in entities created from templates of the configured type. Check this option when the layout (including inline blocks) of new content created from templates should be independent of the library layout.


REQUIREMENTS
------------

This module requires [Template Entities](https://www.drupal.org/project/template_entities) and [Layout Builder Library](https://www.drupal.org/project/layout_library).


INSTALLATION
------------

Install the Template Entities Layout Builder Library module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.

USAGE
-----

Using an example:

1. Create a content type called "Landing pages" configured to use one or more library layouts.
2. Create a template type selecting "Content" for the "Entity type/plugin" option and checking "Landing pages" for the bundle option.
3. Check the checkbox titled "Override Layout Builder Library layout".
4. Create a landing page that uses a library layout.
5. Create a template, using the landing page created above as the source entity.
6. Create a new landing page from the template created in step 5.
7. Go to the "Layout" tab of the new landing page to view and update the fully overridden library layout.

MAINTAINERS
-----------

 * Andy Chapman - https://www.drupal.org/u/chaps2

Supporting organization:

 * Locologic Limited - https://www.locologic.co.uk
